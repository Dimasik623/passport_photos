<?php

class StoreController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','locator'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Store;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Store']))
		{
			$model->attributes=$_POST['Store'];
			$address = (isset($_POST['Store']['postal_code']) && $_POST['Store']['postal_code'] != '') ? $_POST['Store']['postal_code'] : $_POST['Store']['address'].','.$_POST['Store']['city'].','.$_POST['Store']['province'];
			
			$location = $this->get_location($address);
			
			if ($location)
			{
			    $model->lat = $location['lat'];
			    $model->lng = $location['long'];
			    if($model->save())
			        $this->redirect(array('view','id'=>$model->id));
			}

		}

		$this->render('create',array(
			'model'=>$model,
		));
	}
	
	function get_location($address)
	{
	    // get lat and long of the location
		$post = 'http://maps.googleapis.com/maps/api/geocode/json?address='.(str_ireplace(' ','+',$address)).'&sensor=false';
		//var_dump($post);
		$data = CJSON::decode(file_get_contents($post), true);
		//var_dump($data);
		if ($data['results'][0]['geometry']['location']['lat'] != 0 && $data['results'][0]['geometry']['location']['lng'] !=0)
		{
		    return array('lat'=>$data['results'][0]['geometry']['location']['lat'], 'long'=>$data['results'][0]['geometry']['location']['lng']);
		}
		else
		return null;
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Store']))
		{
			$model->attributes=$_POST['Store'];
			// get lat and long of the location
			$address = (isset($_POST['Store']['postal_code']) && $_POST['Store']['postal_code'] != '') ? $_POST['Store']['postal_code'] : $_POST['Store']['address'].','.$_POST['Store']['city'].','.$_POST['Store']['province'];
			
			$location = $this->get_location($address);
			
			if ($location)
			{
			    $model->lat = $location['lat'];
			    $model->lng = $location['long'];
			    if($model->save())
			        $this->redirect(array('view','id'=>$model->id));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Store', array());
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}
	
	public function actionLocator()
	{
	    if (isset($_POST['filter']))
	    {

	        $dia = 6371;//($_POST['units'] == 'm') ? 3959 : 6371;
	        $distance = $_POST['radius'];

	        $post = 'http://maps.googleapis.com/maps/api/geocode/json?address='.(str_ireplace(' ','+',$_POST['address'])).'&sensor=false';
			$data = CJSON::decode(file_get_contents($post), true);
			$lat = $data['results'][0]['geometry']['location']['lat'];
			$lng = $data['results'][0]['geometry']['location']['lng'];

			$temp = Yii::app()->db->createCommand();
			$temp->select('id, lat, lng, name, address, phone_number,city, ( '.$dia.' * acos( cos( radians('.$lat.') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians('.$lng.') ) + sin( radians('.$lat.') ) * sin( radians( lat ) ) ) ) AS distance');
        	$temp->from('store');
        	$temp->having('distance < '.$distance);
        	$temp->order(array('distance ASC'));
        	$temp->limit(20);
        	$stores = $temp->queryAll();
	        //var_dump($stores);
	    }
	    elseif ($_POST['all'])
	    {
	        $temp = Yii::app()->db->createCommand('SELECT `id`, `lat`, `lng`, `name`, `address`, `phone_number`, `city`, "0" AS `distance`
FROM `store`
ORDER BY `city` ASC');
	        //$temp->select('id, lat, lng, name, address, phone_number,city, "0" as distance');
	        //$temp->from('store');
        	//$temp->having('distance < '.$distance);
        	//$temp->order(array('city ASC'));
        	//$temp->limit(20);
        	$stores = $temp->queryAll();
	    }
		
		$this->render('locator',array(
			'dataProvider'=>$stores,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Store('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Store']))
			$model->attributes=$_GET['Store'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Store the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Store::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Store $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='store-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
