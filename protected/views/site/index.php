<?php 
/* @var $this SiteController */
/* @var $model Store */
?>
  
  <div class="masthead"></div>
    <!-- Begin left sidebar -->
    <!---------------------------------------------------------------------------->
    <div id="sidebar1">
    <div class="sidebarContent">
      <a href="onthego.html"><img src="../images/travelling_banner.jpg" width="195" height="246" border="0" alt="Passport photos on the go"/></a></div>
      <div class="sidebarContent" style="background-color: #CCCCCC;">
        <p class="red"><strong>Need a Canadian passport within 24 hours; even on weekends and holidays?</strong></p><p>Ask us, we can tell you how. <br />
        <strong>416-451-8090</strong></p>
      </div>
    <!--<div class="sidebarHead">Quick tips</div>
    <div class="sidebarContent">
    <ul style="padding-left: 10px;">
        <li><strong>Wear dark coloured clothing.</strong><br />
          Light clothing will make you look pale with the required white background.</li>
        <li><strong>Avoid excessive make-up and large shiny jewellery.</strong><bR />
          Reducing glare and hot spots is the key to a good photograph.</li>
        <li><strong>Keep a neutral face when taking your photo.</strong><br />
          To avoid frowning, think pleasant thoughts.</li>
        <li><strong>Keep your mouth closed and lips together when taking your photo.</strong></li>
        <li><strong>Remove glasses whether prescription or tinted.</strong><bR />
          The passport office requires a clear and unobstructed view of your eyes</li>
        <li><strong>Relax, it's only a passport photograph</strong>.<bR />
          It's digital, we can always take it again!!</li>
	</ul>
    </div>
    <div class="sidebarHead">Newest locations</div>
    <div class="sidebarContent">
      <p><strong>Rapid Photo Image Centre</strong><br>
        123 Queen St. West<br>Inside Sheraton Centre Hotel Lower Level<br />Toronto, Ontario<br>416 368 3888</p>
      
      <p><strong>Port Perry - Remedy's Rx Pharmacy</strong><br />
        209 Queen<br />
        Port Perry, Ontario<br />
        905 985 2231</p>
        
        <p><strong>Canada Easy Service</strong><br />
        Splendid China Mall, 2nd Floor<br />
        4675 Steeles Ave East,<br />
        Scarborough, Ontario<br />
         416-499-4467</p>
    </div>-->
  <!-- end #sidebar1 --></div>
    <!-- Begin right sidebar -->
    <!---------------------------------------------------------------------------->
    <div id="sidebar2">
    <div class="sidebarHead">Newest locations</div>
    <div class="sidebarContent" style="background-color: #CCCCCC;">
    
    <?php 
        foreach ($store_list as $store)
        {
            echo '<p style="border-bottom: 1px solid #FFF; padding-bottom: 5px;"><strong class="red">'.$store['name'].'</strong><br />
        '.$store['address'].'<br />
		'.$store['city'].', '.$store['province'].'<br />
		'.$store['postal_code'].'<br />
		'.$store['phone_number'].(($store['phone_ext'] != '') ? ' x'.$store['phone_ext'] :''). '</p>';
        }
    ?>
    <!--
      <p style="border-bottom: 1px solid #FFF; padding-bottom: 5px;"><strong class="red">Loblaws - Richmond Hill</strong><br />
        301 High Tech Road<br />
Richmond Hill, Ontario<br />
L4B 4R2<br />
905 771 1066</p>
      <p style="border-bottom: 1px solid #FFF; padding-bottom: 5px;"><strong class="red">Loblaws - Empress Walk Photolab</strong><br />
        <em>(North York Centre Subway Station<br />
Northbound Platform)</em><br />
5095 Yonge St.<br />
North York, Ontario<br />
M2N 6Z4<br />
416 512 9430 <br />
</p>
<p style="border-bottom: 1px solid #FFF; padding-bottom: 5px;"><strong class="red">Loblaws - Bullock Drive, Markham</strong><br />
  200 Bullock Dr<br />
Markham, Ontario<br />
L3P 6B2<br />
905 294 4922 ext 128<br />
</p>
      <p style="border-bottom: 1px solid #FFF; padding-bottom: 5px;"><strong class="red">Loblaws Superstore - Don Mills</strong><br />
        825 Don Mills Road<br />
        Toronto, Ontario<br />
        M3C 1V4<br />
        416 391 0080<strong><br />
        </strong></p>
        <p style="border-bottom: 1px solid #FFF; padding-bottom: 5px;"><strong class="red">Real Canadian Superstore - <nobr>St. Catherines</nobr></strong><br />
        471 Louth Street<br />
        St. Catherines, Ontario<br />
        L2S 4A2<br />
        905 984 8408</p>
      <p style="border-bottom: 1px solid #FFF; padding-bottom: 5px;"><strong class="red">Rapid Photo Image Centre</strong><br />
        <em>(Inside Sheraton Centre Hotel Lower Level beside the food court)</em><br />
        123 Queen St. West<br />
        Toronto, Ontario<br />
        M5H 2M9<br />
        416 368 3888</p>
          -->
      </div>
    <!--<img src="site_objects/commentaries/bear_e.jpg" width="222" height="223" border="1">-->
    <!-- end #sidebar2 --></div>
    <!-- Begin Main Content Bar -->
    <!---------------------------------------------------------------------------->
    <div id="mainContent">
    <div class="content">
    <h1 class="red">Passport Photos at Your Convenience<br /></h1>
    <p><strong>We take Passport, Citizenship, International & Visa sized photos.</strong></p>
    <p align="left">Come to any one of our convenient locations   throughout the city. We have locations in: <span class="red">Downtown Toronto, Scarborough, Malvern, Chinatown, Brampton, Oakville, Port Perry, St. Catherines and North York</span>.</p>
    <p align="left">We also now offer mobile passport photo services - we come to you. To learn more about this service go to our &quot;<a href="onthego.html" class="red">passports photos on-the-go</a>&quot; page.</p>
    <p align="left">To ensure correct sizing of your photos,   please bring in your application form with the photo specifications.  </p>
    <p><strong>New passport and visa photo services  now available:</strong></p>
    <ul>
      <li>Passport photos on-the-go: emergency and after hours service at your front door or office.</li>
      <li>New official US and UAE (United Arab Emirates) visa photo requirements are now offered in both print and digital photo versions at participating locations.</li>
    </ul>
    <p align="left" class="red"><strong>Every Passport Photo we take   is Guaranteed!!</strong></p>
      <ul>
        <li>Canadian Passport Photos for Adults, Children &amp; Infants </li>
        <li>Canada Passports Renewal <br />
          <em>(<strong>Tip:</strong> renewal photos do not need to be signed)</em></li>
        <li>Canadian Citizenship Photos </li>
        <li>International VISA Photos </li>
        <li>International Passport Photos </li>
        <li>Immigration Photos </li>
        <li>Permanent Resident Photos </li>
        <li>Firearms Acquisition Photos </li>
        <li>University &amp; Regulatory ID Photos</li>
      </ul>
    <p><strong>No appointment is necessary. </strong><strong>Photos are ready while you   wait.</strong></p>
    <p>&nbsp;</p>
    </div>
    <!-- end #mainContent --></div>
    
    