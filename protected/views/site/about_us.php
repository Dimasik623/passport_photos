<?php ?>
<div class="masthead2"></div>

<!-- Begin left sidebar --> 

<!---------------------------------------------------------------------------->

<div id="sidebar1">
  <div class="navHead">About us</div>
  <div class="interiorNav">
    <ul>
      <li></li>
    </ul>
  </div>
  
  <!-- end #sidebar1 --></div>

<!-- Begin Main Content Bar --> 

<!---------------------------------------------------------------------------->

<div id="mainContent2">
<div class="content">
  <h1 class="red">About us</h1>
  <p><em>Welcome to CanadianPassportPhotos.ca</em></p>
  <p>We offer high quality, fast and 100% guaranteed passport & visa photos for consumers through our retail partners.</p>
  <p>We are a Canadian company with over 20 years experience in the photo and portrait industry.</p>
  <p>Through our retail partners we offer Canadian passport, Citizenship, Permanent Resident, Visa &amp; most international sized Visa's and passport photos that your application form requires. </p>
  <p>Whether you need an Indian visa a Chinese passport or an International Driving Permit photo we have the correct size and the expertise to insure that it done right the first time.</p>
  <p>Our digital systems and professional cameras use state of the art technology to produce high quality, fast, fade free photos that are guaranteed to meet all the issuing bodies requirements as well as pleasing our toughest critic, you.</p>
  <p>Our goal is to offer the quickest and best experience possible getting your passport photo taken. Lets face it you probably put getting a passport or visa photo on par with root cannel work! </p>
  <p>We have made it convenient. Our locations are near you. We have made it foolproof.  We know the specs and how to take great pictures. We guarantee our work; our goal is to be 100% accurate the first time.  The only thing worse then getting your photo taken is having to have it redone.</p>
  <p><em>Great value, convenience, knowledgeable & professional staff makes the difference!</em></p>
  <p></p>
</div>
