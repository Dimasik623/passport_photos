<div class="masthead2"></div>

<!-- Begin left sidebar --> 

<!---------------------------------------------------------------------------->

<div id="sidebar1">
  <div class="sidebarHead">Canadian passport photos</div>
  <div class="sidebarContent">
    <ul>
      <li><a href="http://www.ppt.gc.ca/support/faq.aspx" target="_blank" class="red">Passport photos - general</a></li>
      <li><a href="http://www.ppt.gc.ca/can/photos_e.aspx" target="_blank" class="red">Passport photo specification</a></li>
      <li><a href="http://www.cic.gc.ca/english/visit/apply-how.asp" target="_blank" class="red">Visa photos</a></li>
      <li><a href="http://www.cic.gc.ca/english/index.asp" target="_blank" class="red">Permanent-resident  Photos</a></li>
      <li><a href="http://www.ppt.gc.ca/" target="_blank" class="red">Passport office</a></li>
      <li><a href="http://www.ppt.gc.ca/cdn/form.aspx" target="_blank" class="red">Passport application form</a></li>
      <li><a href="http://www.cfc-cafc.gc.ca/info_for-renseignement/renew-renouv/default_e.asp" target="_blank" class="red">Firearm licence photos</a></li>
      <li><a href="http://www.mpss.jus.gov.on.ca/english/police_serv/PISG/private_inv_sec.html" target="_blank" class="red">Private investigators &amp; security guards</a></li>
    </ul>
  </div>
  <div class="sidebarHead">United States passport photos</div>
  <div class="sidebarContent">
    <ul>
      <li><a href="http://travel.state.gov/" target="_blank" class="red">US Passport/Visa - general</a></li>
      <li><a href="http://travel.state.gov/visa/immigrants/immigrants_1340.html" target="_blank" class="red">US Immigrant Visa Photos</a></li>
      <li><a href="http://travel.state.gov/visa/visa_1750.html" target="_blank" class="red">US Visitor Visa photos</a></li>
      <li><a href="http://travel.state.gov/visa/forms/forms_1342.html" class="red">US Visa application forms (including DS160)</a></li>
    </ul>
  </div>
  
  <!-- end #sidebar1 --></div>

<!-- Begin right sidebar --> 

<!---------------------------------------------------------------------------->

<div id="sidebar2">
  <div class="sidebarHead">International travel photos</div>
  <div class="sidebarContent"> United Kingdom
    <ul>
      <li><a href="http://www.ips.gov.uk/cps/rde/xchg/ips_live/hs.xsl/index.htm" target="_blank" class="red">Identity and passport service</a></li>
    </ul>
    India
    <ul>
      <li><a href="http://www.cgitoronto.ca/" target="_blank" class="red">Passport & visa photos </a></li>
      <li><a href="http://www.cgitoronto.ca/Forms/PIOCardForm.pdf" target="_blank" class="red">Indian PIO card application form (pdf)</a></li>
      <li><a href="http://dubaivisa.net/guidelines.aspx" class="red">Dubai Visa guidelines and forms</a></li>
    </ul>
    Japan
    <ul>
      <li><a href="http://www.toronto.ca.emb-japan.go.jp/" target="_blank" class="red">Passport & visa photos </a></li>
      <li><a href="http://www.mofa.go.jp/j_info/visit/visa/index.html" target="_blank" class="red">Guide to Japanese Visas</a></li>
    </ul>
    Pakistan
    <ul>
      <li><a href="http://www.pakmission.ca/frameconsularservices.htm" target="_blank" class="red">Pakistani passport & visa photos</a></li>
    </ul>
    
    <!--<p>United Arab Emirates </p>

    <ul>

      <li><a href="http://www.uae-embassy.com/Visas.html" target="_blank" class="red">Visa requirements for Canadian citizens</a></li>

    </ul>--> 
    
  </div>
  
  <!--<img src="site_objects/commentaries/bear_e.jpg" width="222" height="223" border="1">--> 
  
  <!-- end #sidebar2 --></div>

<!-- Begin Main Content Bar --> 

<!---------------------------------------------------------------------------->

<div id="mainContent">
<div class="content">
  <h1 class="red">Remember the following when you come to have your passport photo taken:</h1>
  </form>
  <ul style="padding-left: 10px;">
    <li>
      <p><strong>Wear dark coloured clothing.</strong><br />
        Light clothing will make you look pale with the required white background.</p>
    </li>
    <li>
      <p><strong>Avoid excessive make-up and large shiny jewellery.</strong><br />
        Reducing glare and hot spots is the key to a good photograph.</p>
    </li>
    <li>
      <p><strong>Keep a neutral face when taking your photo.</strong><br />
        To avoid frowning, think pleasant thoughts.</p>
    </li>
    <li>
      <p><strong>Keep your mouth closed and lips together when taking your photo.</strong></p>
    </li>
    <li>
      <p><strong>Remove glasses whether prescription or tinted.</strong><br />
        The passport office requires a clear and unobstructed view of your eyes</p>
    </li>
    <li>
      <p><strong>Relax, it's only a passport photograph</strong>.<br />
        It's digital, we can always take it again!!</p>
    </li>
  </ul>
  <p>&nbsp;</p>
</div>
