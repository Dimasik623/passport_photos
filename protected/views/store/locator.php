<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3/jquery.min.js"></script>
<?php 
if ($dataProvider !== NULL)
{
    $letters = range('A', 'Z');
    $number = 0;
    foreach($dataProvider as $location)
    {
        if ($location['distance']!='0')
        {
            $dist = ' (~'.number_format($location['distance'],2).(($_POST['units']=='m')?'mi':'km').')';
        }
        $list .= '<div class="partnersListBox">
          <div class="partnersListBox_top"> </div>
          <div class="partnersListBox_bg">
             <div class="partnersListBox_logo">'.$letters[$number].'<!--<img src="'.$picture.'" />--></div>
              <div class="partnersListBox_content">
                  <div class="partnersListBox_title"> '.$location['name'].'</div>
                  <div class="partnersListBox_address"> '.$location['address'].', '.$location['city'].$dist.' <a href="http://maps.google.com/maps?q='.$location['lat'].',%20'.$location['lng'].'" target="_new">get directions</a></div>
                  <div class="partnersListBox_links">
                  
                  '.$location['phone_number'].(($location['phone_ext']!='') ? 'x'.$location['phone_ext'] : '').'
                  </div>
              </div>
              <div class="partnersListBox_clear"> </div>
          </div>
        <div class="partnersListBox_bottom"></div>
    </div>';
        // create javascript for google map
        //$js .= 'var bounds = new google.maps.LatLngBounds();';
        $js .= 'var latlng = new google.maps.LatLng(
            					parseFloat('.$location['lat'].'),
            					parseFloat('.$location['lng'].'));
        		bounds.extend(latlng);
        		createOption("'.$location['name'].'", '.$location['distance'].', '.$number.');
       			createMarker(latlng," '.$location['name'].'"," '.$location['address'].'","'.$letters[$number].'");';
       
		$number ++;
    }
    
}


?>

<h1>Stores</h1>


   <script src="http://maps.googleapis.com/maps/api/js?sensor=false" type="text/javascript"></script>
    <script type="text/javascript">
    //<![CDATA[
    var map;
    var markers = [];
    var infoWindow;
    var locationSelect;

    function load() {
      map = new google.maps.Map(document.getElementById("map"), {
        center: new google.maps.LatLng(43.650078,-79.376915),
        zoom: 6,
        mapTypeId: 'roadmap',
        mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU}
      });
      infoWindow = new google.maps.InfoWindow();
      var bounds = new google.maps.LatLngBounds();
      <?php echo $js; 
      if ($number != 1) {?>
      map.fitBounds(bounds);
      <?php }else{?>
      map.setCenter(bounds.getCenter());
      map.setZoom(15);
      <?php }?>
   }

   function searchLocations() {
     var address = document.getElementById("addressInput").value;
     var geocoder = new google.maps.Geocoder();
     geocoder.geocode({address: address}, function(results, status) {
       if (status == google.maps.GeocoderStatus.OK) {
           
        searchLocationsNear(results[0].geometry.location);
       } else {
         alert(address + ' not found');
       }
     });
   }

   function clearLocations() {
     infoWindow.close();
     for (var i = 0; i < markers.length; i++) {
       markers[i].setMap(null);
     }
     markers.length = 0;

     locationSelect.innerHTML = "";
     var option = document.createElement("option");
     option.value = "none";
     option.innerHTML = "See all results:";
     locationSelect.appendChild(option);
   }



    function createMarker(latlng, name, address, letter) {
      var html = "<b>" + name + "</b> <br/>" + address;
      var marker = new google.maps.Marker({
        map: map,
        position: latlng,
        icon: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld='+ letter+'|ff776b',
        title: name
      });
      google.maps.event.addListener(marker, 'click', function() {
        infoWindow.setContent(html);
        infoWindow.open(map, marker);
      });
      markers.push(marker);
    }

    function createOption(name, distance, num) {
      var option = document.createElement("option");
      option.value = num;
      option.innerHTML = name + "(" + distance.toFixed(1) + ")";
      
    }


    //]]>
  </script>
<script type="text/javascript">

</script>

<?php echo CHtml::beginForm('','post',array('id'=>'formId')); ?>
Address or Postal code: <input type="text" id="addressInput" name="address" size="40"/>
      
Distance:<select id="radiusSelect" name="radius">
		<option value="10" selected>10km</option>
      <option value="25">25km</option>
      <option value="50">50km</option>
      <option value="100">100km</option>
      <option value="200">200km</option>
    </select>

<input type="submit" value="Search" name="filter" />
<br>OR <br><input type="submit" id="AllStores" value="Show All stores" name="all" />
<?php echo CHtml::endForm(); ?>

 <?php if ($dataProvider !== NULL) {?>   
    <div class="partnersListBox">
          <div class="partnersListBox_top"> </div>
          <div class="partnersListBox_bg">
          <div class="partnersListBox_logo"><!--<img src="" />--></div>
              <div id="map" style="width: 500px; height: 400px"></div>
              <div class="partnersListBox_clear"> </div>
          </div>
        <div class="partnersListBox_bottom"></div>
    </div>
<?php }?>
	<?php echo $list ?>
	
	
    
</div>
    
<script type="text/javascript">
<!--
$(function()
		  {
		    var submitActor = null;
		    var $form = $( '#formId' );
		    var $submitActors = $form.find( 'input[type=submit]' );

		    $form.submit( function( event )
		    {
		      if ( null === submitActor )
		      {
		        // If no actor is explicitly clicked, the browser will
		        // automatically choose the first in source-order
		        // so we do the same here
		        submitActor = $submitActors[0];
		      }

		      //alert( submitActor.name );
		      if (submitActor.name == 'filter')
		      {
		    	  var input_name = $.trim($('#addressInput').val());
		    	  if (input_name  === '') {
		    		  $('#addressInput').attr('style', 'border-color:#FF0000;');
		    		  //$('#addressInput').val('Cannot be empty');
		    	      alert('Location can not be empty.');
		    	      return false;
		    	  }
		      }

		      //return false;
		    });

		    $submitActors.click( function( event )
		    {
		      submitActor = this;
		    });

		  } );
//Bind the event handler to the "submit" JavaScript event
/*
$('form').submit(function () {

  // Get the Login Name value and trim it
  var name = $.trim($('#addressInput').val());

  // Check if empty of not
  if ($('#AllStores').val() === '')
  {
	  if (name  === '') {
		  $('#addressInput').attr('style', 'border-color:#FF0000;');
		  //$('#addressInput').val('Cannot be empty');
	      //alert('Location can not be empty.');
	      return false;
	  }
  }
});
*/
//-->
<!--
load();
//-->
</script>